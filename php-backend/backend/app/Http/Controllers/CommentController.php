<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Http\Resources\Comment as CommentResource;
use App\Http\Resources\CommentCollection;
use Illuminate\Http\Request;

class CommentController extends Controller
{
	public function index()
    {
        return new CommentCollection(Comment::all());
    }

    public function store(Request $request)
    {
    	$comment = Comment::create($request->all());

        return (new CommentResource($comment))
                ->response()
                ->setStatusCode(201);
    }
}
