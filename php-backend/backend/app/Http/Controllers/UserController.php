<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;

use App\User;
use App\Http\Resources\User as UserResource;
use App\Http\Resources\UserCollection;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class UserController extends Controller
{
	// Register user
    public function userPostRegistration(Request $request) {

        // validate form fields
        $request->validate([
                'name'        =>      'required',
                'email'             =>      'required|email',
                'password'          =>      'required|min:6',
                'confirm_password'  =>      'required|same:password',
                'interest'          =>      'required'
            ]);

        $input          =           $request->all();

        // if validation success then create an input array
        $inputArray      =           array(
            'name'        =>      $request->name,
            'email'             =>      $request->email,
            'password'          =>      Hash::make($request->password),
            'interest'             =>      $request->interest
        );

        // register user
        $user           =           User::create($inputArray);

        // if registration success then return with success message
        if(!is_null($user)) {
            return (new UserResource($user))
                ->response()
                ->setStatusCode(201);
        }

        // else return with error message
        else {
            return back()->with('error', 'Whoops! some error encountered. Please try again.');
        }
    }

    // Login User
    public function userPostLogin(Request $request) {

        $request->validate([
            "email"           =>    "required|email",
            "password"        =>    "required|min:6"
        ]);

        $userCredentials = $request->only('email', 'password');

        // check user using auth function
        if (Auth::attempt($userCredentials)) {
            return response($request->email)
                ->setStatusCode(200);
        }

        else {
            return back()->with('error', 'Whoops! invalid username or password.');
        }
    }

    public function getUserByEmail(Request $request) {
        $email =$request->input('email');
        $user = User::where('email', $email)->first();

        return $user;
    }

    public function updateUser(Request $request){

       $reqdata = $request->all(); 

        $reqdata['created_at'] = date('Y-m-d');
        $reqdata['updated_at'] = date('Y-m-d');

         $user= User::where('id',$request->id)->update($reqdata);

        if ($user) {

            return 'true';

        }else{

            return 'false';

        }

    } 
}
