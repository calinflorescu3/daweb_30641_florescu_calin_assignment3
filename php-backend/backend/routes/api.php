<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/comment', 'CommentController@store');
Route::get('/comments', 'CommentController@index');

Route::post('/user-store', 'UserController@userPostRegistration');

Route::post('/user', 'UserController@getUserByEmail');

Route::post('/login', 'UserController@userPostLogin');

Route::put('/user', 'UserController@updateUser');