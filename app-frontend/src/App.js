import React, { Component, Suspense } from "react";
import LandingPage from "./components/LandingPage/landingPageComponent";

class App extends Component {
  render() {
    return (
      <Suspense fallback="loading">
        <div>
          <LandingPage />
        </div>
      </Suspense>
    );
  }
}

export default App;
