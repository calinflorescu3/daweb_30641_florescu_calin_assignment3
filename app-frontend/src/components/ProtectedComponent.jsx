import React from "react";
import { Redirect } from "react-router-dom";

export default function ProtectedComponent({ children }) {
  const isLogged = sessionStorage.getItem("isLogged");

  if (isLogged === "true") {
    console.log("I am here");
    return children;
  } else {
    alert("You have to login first");
    return <Redirect to="/" />;
  }
}
