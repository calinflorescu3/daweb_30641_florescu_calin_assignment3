import React, { useState, useEffect } from "react";
import { Card, CardBody, Button, CardTitle, CardHeader } from "reactstrap";
import EditModal from "./EditModal";
import "./profile.css";

const Profile = ({ t, i18n }) => {
  const [data, setData] = useState({
    name: "",
    interest: "",
    email: "",
  });
  const [modal, setModal] = useState({
    willOpen: false,
    type: "",
  });
  const [isLoaded, setLoaded] = useState(false);

  useEffect(() => {
    fetch("http://localhost:8000/api/user", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        email: sessionStorage.getItem("email"),
      }),
    })
      .then((response) => {
        if (response && response.status === 200) {
          return response.json();
        }
        console.log("Error");
      })
      .then((data) => {
        setData(data);
        setLoaded(true);
      })
      .catch((e) => console.log(e));
  }, [modal]);

  const handleClick = (event) => {
    setModal({
      willOpen: !modal.willOpen,
      type: event.target.id,
    });
  };

  const toggleOff = () => {
    setModal({
      ...modal,
      willOpen: !modal.willOpen,
    });
  };

  return (
    <>
      <div className={"profile-container"}>
        <h1 className={"news-header"}>{t("title-student")}</h1>
        <div className={"profile-body"}>
          <img src="./Poza.jpg" className={"profile-image"} />
          <div className={"user-info"}>
            {isLoaded ? (
              <>
                {" "}
                <div className={"left-panel"}>
                  <Card className="text-center">
                    <CardHeader className={"contact-header"}>
                      {t("name-coordonator")}
                      <Button
                        id="name-coordonator"
                        color="success"
                        className="edit-button"
                        onClick={handleClick}
                      >
                        Edit
                      </Button>
                    </CardHeader>
                    <CardBody>
                      <CardTitle className={"contact-text"}>
                        {data.name}
                      </CardTitle>
                    </CardBody>
                  </Card>
                  <Card className="text-center">
                    <CardHeader className={"contact-header"}>
                      {t("email-contact")}
                      <Button
                        id="email-contact"
                        color="success"
                        className="edit-button"
                        onClick={handleClick}
                      >
                        Edit
                      </Button>
                    </CardHeader>
                    <CardBody>
                      <CardTitle className={"contact-text"}>
                        {data.email}
                      </CardTitle>
                    </CardBody>
                  </Card>
                </div>
                <div className={"left-panel"}>
                  <Card className="text-center">
                    <CardHeader className={"contact-header"}>
                      {t("password")}
                      <Button
                        id="password"
                        color="success"
                        className="edit-button"
                        onClick={handleClick}
                      >
                        Edit
                      </Button>
                    </CardHeader>
                    <CardBody>
                      <CardTitle className={"contact-text"}>Parola</CardTitle>
                    </CardBody>
                  </Card>
                  <Card className="text-center">
                    <CardHeader className={"contact-header"}>
                      {t("interest")}
                      <Button
                        id="interest"
                        color="success"
                        className="edit-button"
                        onClick={handleClick}
                      >
                        Edit
                      </Button>
                    </CardHeader>
                    <CardBody>
                      <CardTitle className={"contact-text interest-card"}>
                        {data.interest}
                      </CardTitle>
                    </CardBody>
                  </Card>
                </div>{" "}
              </>
            ) : null}
          </div>
        </div>
      </div>
      <EditModal
        open={modal.willOpen}
        toggle={toggleOff}
        type={modal.type}
        data={data}
        t={t}
      />
    </>
  );
};

export default Profile;
