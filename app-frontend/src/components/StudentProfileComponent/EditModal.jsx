import React, { useState } from "react";
import {
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Input,
  FormGroup,
  Label,
  CustomInput,
} from "reactstrap";

export default function EditModal({ open, toggle, t, type, data }) {
  const [field, setData] = useState("");

  const handleChange = (event) => {
    setData(event.target.value);
  };

  const handleClick = (event) => {
    event.preventDefault();

    let sendData = {};

    const fieldType = type.split("-")[0];
    if (field && fieldType === "interest") {
      sendData = { ...data, [fieldType]: `${data[fieldType]} ${field}` };
    } else if (field && data[fieldType] !== field) {
      sendData = { ...data, [fieldType]: field };
    }

    fetch("http://localhost:8000/api/user", {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(sendData),
    }).then((response) => {
      if (response && response.status === 200) {
        toggle();
      }
      console.log("Error");
    });
  };

  return (
    <Modal isOpen={open} toggle={toggle}>
      <ModalHeader toggle={toggle}>{t(`${type}`)}</ModalHeader>
      <ModalBody>
        {type !== "interest" ? (
          <InputGroup>
            <InputGroupAddon addonType="prepend">
              <InputGroupText></InputGroupText>
            </InputGroupAddon>
            <Input placeholder={t(`${type}`)} onChange={handleChange} />
          </InputGroup>
        ) : (
          <FormGroup>
            <Label for="exampleCustomSelect">{t(`${type}`)}</Label>
            <CustomInput
              type="select"
              id="exampleCustomSelect"
              name="customSelect"
              onChange={handleChange}
            >
              <option value="Web Development">Web Development</option>
              <option value="AI">AI</option>
              <option value="Natural Language Processing">
                Natural Language Processing
              </option>
              <option value="APIS">API'S</option>
              <option value="Mathematics">Mathematics</option>
            </CustomInput>
          </FormGroup>
        )}
      </ModalBody>
      <ModalFooter>
        <Button color="success" onClick={handleClick}>
          Edit
        </Button>{" "}
        <Button color="secondary" onClick={toggle}>
          Cancel
        </Button>
      </ModalFooter>
    </Modal>
  );
}
