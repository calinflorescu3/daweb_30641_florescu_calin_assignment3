import React, { useState, useContext } from "react";
import { Button, Form, FormGroup, Label, Input } from "reactstrap";
import { Redirect } from "react-router-dom";

export default function Login() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [redirectApp, setredirectApp] = useState(false);

  const handleChangeEmail = (e) => {
    setEmail(e.target.value);
  };

  const handleChangePassword = (e) => {
    setPassword(e.target.value);
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    fetch("http://localhost:8000/api/login", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        email,
        password,
      }),
    })
      .then((response) => {
        console.log(response);
        if (response.status === 200) {
          return response.text();
        } else {
          console.log("Error");
        }
      })
      .then((data) => {
        sessionStorage.setItem("isLogged", "true");
        sessionStorage.setItem("email", data);
        setredirectApp(true);
      })
      .catch((e) => console.log(e));
  };

  return (
    <>
      {redirectApp ? <Redirect to="/app" /> : null}
      <Form className={"form-body"} onSubmit={handleSubmit}>
        <p className={"home-title"}>Login</p>
        <FormGroup>
          <Label for="exampleEmail">Email</Label>
          <Input
            type="email"
            name="email"
            id="exampleEmail"
            placeholder="email"
            onChange={handleChangeEmail}
          />
        </FormGroup>
        <FormGroup>
          <Label for="examplePassword">Password</Label>
          <Input
            type="password"
            name="password"
            id="examplePassword"
            placeholder="password"
            onChange={handleChangePassword}
          />
        </FormGroup>
        <Button className={"login-button"} type="submit">
          Login
        </Button>
      </Form>
    </>
  );
}
