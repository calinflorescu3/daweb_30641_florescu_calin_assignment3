import React, { useState, useContext } from "react";
import { Button, Form, FormGroup, Label, Input } from "reactstrap";
import { ApplicationContext } from "../../context/ApplicationContext";
import "./Home.css";

export default function Register() {
  const [data, setData] = useState({
    name: "",
    email: "",
    password: "",
    confirm_password: "",
    interest: "empty",
  });
  const { setRedirectLogin } = useContext(ApplicationContext);

  const handleClickText = () => {
    setRedirectLogin(true);
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    console.log(data);
    fetch("http://localhost:8000/api/user-store", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ ...data }),
    })
      .then((response) => {
        console.log(response);
        if (response && response === 201) {
          return response.json();
        }
        console.log(response);
        return;
      })
      .then((data) => {
        console.log(data);
        setRedirectLogin(true);
      })
      .catch((e) => console.log(e));
  };

  const handleChange = (e) => {
    setData({
      ...data,
      [e.target.id]: e.target.value,
    });
  };

  return (
    <Form className={"form-body"} onSubmit={handleSubmit}>
      <p className={"home-title"}>Sign Up</p>
      <FormGroup>
        <Label for="exampleEmail">Name</Label>
        <Input
          type="name"
          name="name"
          id="name"
          placeholder="name"
          onChange={handleChange}
        />
      </FormGroup>
      <FormGroup>
        <Label for="exampleEmail">Email</Label>
        <Input
          type="email"
          name="email"
          id="email"
          placeholder="email"
          onChange={handleChange}
        />
      </FormGroup>
      <FormGroup>
        <Label for="examplePassword">Password</Label>
        <Input
          type="password"
          name="password"
          id="password"
          placeholder="password"
          onChange={handleChange}
        />
      </FormGroup>
      <FormGroup>
        <Label for="examplePassword">Repeat Password</Label>
        <Input
          type="password"
          name="password"
          id="confirm_password"
          placeholder="password"
          onChange={handleChange}
        />
      </FormGroup>
      <p className={"message"} onClick={handleClickText}>
        Already have an account?
      </p>
      <Button className={"login-button"} type="submit">
        Register
      </Button>
    </Form>
  );
}
