import React, { useState, useContext } from "react";
import Register from "./Register";
import { ApplicationContext } from "../../context/ApplicationContext";
import Login from "./Login";
import "./Home.css";

export default function Home() {
  const { redirectLogin } = useContext(ApplicationContext);

  return (
    <div className={"container"}>
      {redirectLogin ? <Login /> : <Register />}
    </div>
  );
}
