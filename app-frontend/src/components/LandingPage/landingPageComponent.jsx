import React, { useState } from "react";
import { Row, Col } from "reactstrap";
import Profile from "../StudentProfileComponent/profile";
import News from "../NewsComponent/news";
import Project from "../ProjectComponent/project";
import Contact from "../ContactComponent/contact";
import Coordonator from "../CoordonatorComponent/coordonator";
import { useTranslation } from "react-i18next";
import "./landingPage.css";

export default function landingPageComponent() {
  const [activeScreen, setActiveScreen] = useState("Acasa");
  const [language, setLanguage] = useState("ro");
  const { t, i18n } = useTranslation();

  const homeHandleClick = () => {
    setActiveScreen("Acasa");
  };

  const newsHandleClick = () => {
    setActiveScreen("Noutati");
  };

  const aboutHandleClick = () => {
    setActiveScreen("Despre");
  };

  const profileHandleClick = () => {
    setActiveScreen("Profil");
  };

  const coordonatorHandleClick = () => {
    setActiveScreen("Coordonator");
  };

  const contactHandleClick = () => {
    setActiveScreen("Contact");
  };

  const handleChangeLanguage = () => {
    let newLanguage;
    if (language === "ro") {
      newLanguage = "en";
    }

    if (language === "en") {
      newLanguage = "ro";
    }

    i18n.changeLanguage(newLanguage);
    setLanguage(newLanguage);
  };

  return (
    <div className={"container"}>
      <Row className={"row"}>
        <Col className={"col"} onClick={homeHandleClick}>
          {t("nav-button-home")}
        </Col>
        <Col className={"col"} onClick={newsHandleClick}>
          {t("nav-button-news")}
        </Col>
        <Col className={"col"} onClick={aboutHandleClick}>
          {t("nav-button-project")}
        </Col>
        <Col className={"col"} onClick={profileHandleClick}>
          {t("nav-button-student")}
        </Col>
        <Col className={"col"} onClick={coordonatorHandleClick}>
          {t("nav-button-coordonator")}
        </Col>
        <Col className={"col"} onClick={contactHandleClick}>
          {t("nav-button-contact")}
        </Col>
      </Row>
      <Row className={"body-container"}>
        {activeScreen === "Acasa" ? (
          <div className={"welcome"}>{t("title-home")}</div>
        ) : null}
        {activeScreen === "Noutati" ? <News t={t} i18n={i18n} /> : null}
        {activeScreen === "Despre" ? <Project t={t} i18n={i18n} /> : null}
        {activeScreen === "Profil" ? <Profile t={t} i18n={i18n} /> : null}
        {activeScreen === "Coordonator" ? (
          <Coordonator t={t} i18n={i18n} />
        ) : null}
        {activeScreen === "Contact" ? <Contact t={t} i18n={i18n} /> : null}
      </Row>
      <div className={"decor"} onClick={handleChangeLanguage}>
        {t("change-language")}
        {language === "en" ? (
          <img className={"flag-image"} src="./romania.jpg" />
        ) : (
          <img className={"flag-image"} src="./british.png" />
        )}
      </div>
    </div>
  );
}
