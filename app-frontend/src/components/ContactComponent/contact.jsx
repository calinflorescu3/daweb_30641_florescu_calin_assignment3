import React, { useState } from "react";
import {
  Card,
  CardHeader,
  CardBody,
  CardTitle,
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Input
} from "reactstrap";
import "./contact.css";

export default function contact({ t, i18n }) {
  const [modal, setModal] = useState(false);
  const [message, setMessage] = useState("");
  const [email, setEmail] = useState("");

  const handleChange = e => {
    setMessage(e.target.value);
  };

  const updateEmail = e => {
    console.log(e.target.value);
    setEmail(e.target.value);
  };

  const toggle = () => {
    setModal(!modal);
  };

  const sendMessage = () => {
    fetch("http://localhost:5000/message", {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        message,
        email
      })
    })
      .then(response => response.text())
      .then(data => {
        console.log(data);
        setModal(!modal);
      })
      .catch(er => console.error(er));
  };

  return (
    <div className={"news-container"}>
      <div className={"news-header"}>{t("title-contact")}</div>
      <div className={"contact-body"}>
        <Card className="text-center">
          <CardHeader className={"contact-header"}>
            {t("email-contact")}
          </CardHeader>
          <CardBody>
            <CardTitle className={"contact-text"}>
              calinflorescu3@gmail.com
            </CardTitle>
          </CardBody>
        </Card>

        <Card className="text-center">
          <CardHeader className={"contact-header"}>
            {t("address-contact")}
          </CardHeader>
          <CardBody>
            <CardTitle className={"contact-text"}>
              Strada X, Bloc Y, Apartament Z, oras Cluj-Napoca, judet Cluj
            </CardTitle>
          </CardBody>
        </Card>

        <Card className="text-center">
          <CardHeader className={"contact-header"}>
            {t("phone-contact")}
          </CardHeader>
          <CardBody>
            <CardTitle className={"contact-text"}>0751322081</CardTitle>
          </CardBody>
        </Card>
        <Button
          color="danger"
          onClick={toggle}
          className={"send-message-button"}
        >
          {t("send-email")}
        </Button>
        <Modal isOpen={modal} toggle={toggle}>
          <ModalHeader toggle={toggle}>{t("send-email")}</ModalHeader>
          <ModalBody>
            <Input
              type="email"
              name="email"
              id="example-email"
              placeholder="email"
              onChange={updateEmail}
            ></Input>
            <Input
              type="textarea"
              name="text"
              id="example-text"
              onChange={event => handleChange(event)}
            />
          </ModalBody>
          <ModalFooter>
            <Button color="danger" onClick={sendMessage}>
              {t("send-email")}
            </Button>{" "}
            <Button color="secondary" onClick={toggle}>
              Cancel
            </Button>
          </ModalFooter>
        </Modal>
      </div>
    </div>
  );
}
