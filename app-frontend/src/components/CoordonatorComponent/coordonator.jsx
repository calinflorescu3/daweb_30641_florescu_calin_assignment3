import React from "react";
import { Card, CardHeader, CardBody, CardTitle } from "reactstrap";
import "./coordonator.css";

export default function coordonator({ t, i18n }) {
  return (
    <div className={"news-container"}>
      <div className={"news-header"}>{t("title-coordonator")}</div>
      <div className={"coordonator-container"}>
        <Card className="text-center">
          <CardHeader className={"contact-header"}>
            {t("name-coordonator")}
          </CardHeader>
          <CardBody>
            <CardTitle className={"contact-text"}>Nume prenume</CardTitle>
          </CardBody>
        </Card>

        <Card className="text-center">
          <CardHeader className={"contact-header"}>
            {t("function-coordonator")}
          </CardHeader>
          <CardBody>
            <CardTitle className={"contact-text"}>
              Functia profesorului
            </CardTitle>
          </CardBody>
        </Card>

        <Card className="text-center">
          <CardHeader className={"contact-header"}>
            {t("research-directions-coordonator")}
          </CardHeader>
          <CardBody>
            <CardTitle className={"contact-text"}>
              Directiile de cercetare ale profesorului
            </CardTitle>
          </CardBody>
        </Card>
      </div>
    </div>
  );
}
