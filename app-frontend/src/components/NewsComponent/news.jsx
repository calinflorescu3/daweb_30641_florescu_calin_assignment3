import React, { useState, useEffect } from "react";
import { Card, CardHeader, CardBody, CardText, CardTitle } from "reactstrap";
import "./news.css";

export default function News({ t, i18n }) {
  const [news, setNews] = useState({});
  const [isLoaded, setLoaded] = useState(false);

  useEffect(() => {
    fetch("http://localhost:5000/news", {
      method: "GET",
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(response => response.json())
      .then(data => {
        setNews(data);
        setLoaded(true);
      });
  }, []);

  return (
    <div className={"news-container"}>
      <div className={"news-header"}>{t("title-news")}</div>
      {isLoaded ? (
        <div className={"news-body"}>
          {news.articles.article.map((item, index) => {
            return (
              <Card key={index} className={"news-card"}>
                <CardHeader className={"news-card-title"}>
                  {item.heading} {item.date}
                </CardHeader>
                <CardBody className={"news-card-body"}>
                  <CardTitle>{item.body.title}</CardTitle>
                  <CardText>{item.body.description}</CardText>
                </CardBody>
              </Card>
            );
          })}
        </div>
      ) : (
        <p>Loading</p>
      )}
    </div>
  );
}
