import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Home from "./HomeComponent/Home";
import ApplicationProvider from "../context/ApplicationContext";
import App from "../App";
import ProtectedComponent from "./ProtectedComponent";

export default () => {
  return (
    <>
      <Router>
        <Switch>
          <Route
            exact
            path="/"
            render={() => <ApplicationProvider children={<Home />} />}
          />
          <Route
            exact
            path="/app"
            render={() => <ProtectedComponent children={<App />} />}
          />
        </Switch>
      </Router>
    </>
  );
};
