import React, { createContext, useState } from "react";

export const ApplicationContext = createContext();

export default ({ children }) => {
  const [redirectLogin, setRedirectLogin] = useState(false);

  return (
    <ApplicationContext.Provider value={{ redirectLogin, setRedirectLogin }}>
      {children}
    </ApplicationContext.Provider>
  );
};
